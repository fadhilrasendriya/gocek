# Generated by Django 3.1.1 on 2020-11-16 15:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hiburan', '0002_auto_20201116_2214'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hiburan',
            name='link_foto',
        ),
        migrations.AddField(
            model_name='hiburan',
            name='foto',
            field=models.ImageField(null=True, upload_to='images/'),
        ),
    ]
