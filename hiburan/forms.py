from django.forms import Form
from django import forms
from .models import *

class CreateHiburan(forms.Form):
    update = False
    nama = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama tempat?',
        'type' : 'text',
        'required' : True,
        'disable' : update,
    }))
    status = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Sesuai Protokol Covid?',
        'type' : 'text',
        'required' : True,
    }))
    budget = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Berapa budgetnya',
        'type' : 'text',
        'required' : True,
    }))
    kontak = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nomor kontaknya?',
        'type' : 'text',
        'required' : True,
    }))
    alamat = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Dimana tempatnya?',
        'type' : 'text',
        'required' : True,
        'disable' : update,
    }))
    
    jam_buka = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Pukul Buka?',
        'type' : 'text',
        'required' : True,
    }))
    
    jam_tutup = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Pukul Tutup?',
        'type' : 'text',
        'required' : True,
    }))

    foto = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Linknya Boleh Di-Upload Disini',
        'type' : 'text',
        'required' : True,
    }))

    class Meta:
        model = Hiburan
        fields = ['nama','status','budget','kontak','alamat','foto','jam_buka','jam_tutup',]
    # foto = forms.ImageField()
    # jam_buka = forms.TimeField(widget=forms.TimeInput)
    # jam_tutup = forms.TimeField(widget=forms.TimeInput)

class CreateFoto(forms.Form):
    link_foto = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Linknya Dong',
        'type' : 'text',
        'required' : True,
    }))
    class Meta:
        model = Fotos
        fields = ['link_foto']