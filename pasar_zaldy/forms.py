from django import forms
from .models import Penjual, Pasar

class PasarForms(forms.ModelForm):
    namaPasar = forms.CharField(label='Nama Pasar', 
                    widget=forms.TextInput(attrs={"placeholder": "co: Pasar Depok Jaya"}), )
    hotlinePasar = forms.IntegerField(label='Hotline Pasar', 
                    widget=forms.TextInput(attrs={"placeholder": "0856xxxxxx"}))
    jambukaPasar = forms.TimeField(label='Jam Buka Pasar', 
                    widget=forms.TextInput(attrs={"placeholder": "co: 08:00"}))
    jamtutupPasar = forms.TimeField(label='Jam Tutup Pasar', 
                    widget=forms.TextInput(attrs={"placeholder": "co: 21:00"}))
    fotoPasar = forms.CharField(label='Link Foto Pasar', 
                    widget=forms.TextInput(attrs={"placeholder": "Copy Paste Link disini"}))
    alamatPasar = forms.CharField(label='Alamat Pasar', 
                    widget=forms.TextInput(attrs={"placeholder": "Jalan Margonda Raya"}))
    
    class Meta:
        model = Pasar
        fields = [
            'namaPasar', 'hotlinePasar', 'statuscovidPasar', 'jambukaPasar',
            'jamtutupPasar', 'fotoPasar', 'alamatPasar',
        ]
        
    
class PenjualForms(forms.ModelForm):
    namaPenjual = forms.CharField(label='Nama Pasar', 
                    widget=forms.TextInput(attrs={"placeholder": "Nama"}),)
    kontakPenjual = forms.CharField(label='Kontak', 
                    widget=forms.TextInput(attrs={"placeholder": "0867xxxxxxx"}), )
    jenisJualan = forms.CharField(label='Jenis Jualan', 
                    widget=forms.TextInput(attrs={"placeholder": "Sayur, Obat, Makanan"}), )
    
    

    class Meta:
        model = Penjual
        fields = [
            'namaPenjual',
            'kontakPenjual',
            'jenisJualan',
        ]