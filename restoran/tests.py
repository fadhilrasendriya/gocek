from django.test import TestCase, Client
from .models import Restoran, Makanan
from .forms import RestoranForm, MakananForm
from . import views
from django.urls.base import resolve


import tempfile
import datetime

class RestoranTest(TestCase):
    def dummy_restoran(self):
        test = 'Anonymous'
        test_time_open = datetime.time(0, 0, 0)
        test_time_close = datetime.time(1, 0, 0)
        image = tempfile.NamedTemporaryFile(suffix='.jpg').name
        obj = Restoran.objects.create(nama=test, alamat=test, time_open=test_time_open, time_close=test_time_close, picture=image)
        return obj

    def dummy_makanan(self):
        test = 'IniMakanan'
        price = 1
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        restoran = self.dummy_restoran()
        obj = Makanan.objects.create(nama=test, harga=price, restoran=restoran, picture=image)
        return obj

    def test_model_restoran_can_create(self):
        test = 'Anonymous'
        test_time_open = datetime.time(0, 0, 0)
        test_time_close = datetime.time(1, 0, 0)
        image = tempfile.NamedTemporaryFile(suffix='.jpg').name
        Restoran.objects.create(nama=test, alamat=test, time_open=test_time_close, time_close=test_time_close, picture=image)
        object_counter = Restoran.objects.all().count()
        self.assertEqual(object_counter, 1)

    def test_model_makanan_can_create(self):
        test = 'Anonymous'
        price = 1
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        restoran = Restoran.objects.create(nama=test, alamat=test, time_open=datetime.time(0,0,0), time_close=datetime.time(1,0,0), picture=image)
        Makanan.objects.create(nama=test, harga=price, restoran=restoran, picture=image)
        object_counter = Makanan.objects.all().count()
        self.assertEqual(object_counter, 1)

    def test_restoran_url_exists(self):
        response = Client().get('/restoran/')
        self.assertEqual(response.status_code, 200)

    def test_restoran_main_using_index_function(self):
        found = resolve('/restoran/')
        self.assertEqual(found.func, views.index)

    def test_add_restoran_url_exists(self):
        response = Client().get('/restoran/add/')
        self.assertEqual(response.status_code, 200)

    def test_add_restoran_using_add_function(self):
        found = resolve('/restoran/add/')
        self.assertEqual(found.func, views.add)

    def test_detail_url_exists(self):
        self.dummy_restoran()
        response = Client().get('/restoran/detail/1')
        self.assertEqual(response.status_code, 200)

    def test_makanan_exists_in_detail(self):
        self.dummy_makanan()
        response = Client().get('/restoran/detail/1')
        html_response = response.content.decode('utf8')
        self.assertIn('IniMakanan', html_response)

    def test_restoran_exists(self):
        self.dummy_restoran()
        response = Client().get('/restoran/detail/1')
        html_response = response.content.decode('utf8')
        self.assertIn('Anonymous', html_response)

    def test_detail_using_detail_function(self):
        self.dummy_restoran()
        found = resolve('/restoran/detail/1')
        self.assertEqual(found.func, views.detail)

    def test_update_url_exists(self):
        self.dummy_restoran()
        response = Client().get('/restoran/update/1')
        self.assertEqual(response.status_code, 200)

    def test_update_using_update_function(self):
        self.dummy_restoran()
        found = resolve('/restoran/update/1')
        self.assertEqual(found.func, views.update)

    def test_add_makanan_using_add_food_function(self):
        self.dummy_restoran()
        found = resolve('/restoran/add-food/1')
        self.assertEqual(found.func, views.add_food)



