from django.shortcuts import render, HttpResponseRedirect, get_object_or_404, reverse
from .forms import RestoranForm, MakananForm
from .models import Restoran, Makanan


def index(request):
    list_restoran = Restoran.objects.all()
    context = {'list_restoran': list_restoran}
    return render(request, 'restoran/index.html', context)

def add(request):
    form = RestoranForm()
    if request.method == 'POST':
        form = RestoranForm(request.POST, request.FILES)
        if form.is_valid():
            nama = form.cleaned_data.get('nama')
            alamat = form.cleaned_data.get('alamat')
            picture = form.cleaned_data.get('picture')
            kategori = form.cleaned_data.get('kategori')
            jenis_pesanan = form.cleaned_data.get('jenis_pesanan')
            time_open = form.cleaned_data.get('time_open')
            time_close = form.cleaned_data.get('time_close')
            telepon = form.cleaned_data.get('telepon')
            obj = Restoran.objects.create(nama=nama,telepon=telepon, alamat=alamat,picture=picture, jenis_pesanan=jenis_pesanan, time_open=time_open, time_close=time_close, kategori=kategori)
            obj.save()
        else:
            print('invalid form')
        return HttpResponseRedirect('/restoran/')
    context = {'form': form}
    return render(request, 'restoran/add.html', context)

def add_food(request, id):
    form = MakananForm()
    if request.method == 'POST':
        form = MakananForm(request.POST, request.FILES)
        if form.is_valid():
            nama = form.cleaned_data.get('nama')
            harga = form.cleaned_data.get('harga')
            picture = form.cleaned_data.get('picture')
            restoran_id = request.POST['restoran_id']
            restoran = get_object_or_404(Restoran, id=restoran_id)
            obj = Makanan.objects.create(nama=nama, harga=harga, picture=picture, restoran=restoran)
            obj.save()
        return HttpResponseRedirect('/restoran/detail/' + id)
    context = {'form': form, 'restoran_id': id}
    return render(request, 'restoran/add_food.html', context)

def detail(request, id):
    restoran = get_object_or_404(Restoran, id=id)
    context = {'restoran': restoran}
    foods = Makanan.objects.all()
    food_exists = False
    if foods:
        context['foods'] = foods
        food_exists = True
    context['food_exists'] = food_exists
    return render(request, 'restoran/detail.html', context)

def update(request, id):
    restoran = Restoran.objects.get(id=id)
    form = RestoranForm()
    if request.method == 'POST':
        form = RestoranForm(request.POST, request.FILES)
        if form.is_valid() or True:
            picture = form.cleaned_data.get('picture')
            kategori = form.cleaned_data.get('kategori')
            time_open = form.cleaned_data.get('time_open')
            time_close = form.cleaned_data.get('time_close')
            telepon = form.cleaned_data.get('telepon')
            restoran.picture = picture
            restoran.kategori = kategori
            restoran.time_open = time_open
            restoran.time_close = time_close
            restoran.telepon = telepon
            restoran.save()
        return HttpResponseRedirect('/restoran/detail/' + id)
    context = {'form': form, 'restoran': restoran}

    return render(request, 'restoran/update.html', context)
