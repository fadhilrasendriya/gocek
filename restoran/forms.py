from django.forms import Form
from django import forms
from .models import Restoran, Makanan

class RestoranForm(Form):
    nama = forms.CharField(max_length=50)
    alamat = forms.CharField(widget=forms.Textarea)
    kategori = forms.CharField(max_length=50)
    jenis_pesanan = forms.CharField(max_length=50)
    telepon = forms.CharField(max_length=15)
    picture = forms.ImageField()
    time_open = forms.TimeField(widget=forms.TimeInput)
    time_close = forms.TimeField(widget=forms.TimeInput)


class MakananForm(Form):
    nama = forms.CharField(max_length=50)
    harga = forms.IntegerField()
    picture = forms.ImageField()



