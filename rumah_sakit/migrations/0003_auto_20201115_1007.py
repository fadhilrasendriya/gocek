# Generated by Django 3.1.1 on 2020-11-15 03:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rumah_sakit', '0002_auto_20201113_1444'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rumahsakit',
            name='alamat',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='rumahsakit',
            name='covid',
            field=models.TextField(choices=[('yesP', 'Menerima '), ('noP', 'Tidak Menerima')], max_length=10),
        ),
        migrations.AlterField(
            model_name='rumahsakit',
            name='nama_rs',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='rumahsakit',
            name='test_rs',
            field=models.CharField(choices=[('yesT', 'Menerima'), ('noT', 'Tidak Menerima')], max_length=10),
        ),
        migrations.CreateModel(
            name='Dokter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_dokter', models.CharField(max_length=25)),
                ('spesialis', models.CharField(max_length=25)),
                ('layanan_start', models.TimeField()),
                ('layanan_end', models.TimeField()),
                ('phone_number', models.IntegerField()),
                ('rs', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rumah_sakit.rumahsakit')),
            ],
        ),
    ]
