# Generated by Django 3.1.1 on 2020-11-16 09:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rumah_sakit', '0007_auto_20201115_1914'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rumahsakit',
            name='covid',
            field=models.TextField(choices=[('Iya', 'Menerima '), ('Tidak', 'Tidak Menerima')], max_length=20),
        ),
        migrations.AlterField(
            model_name='rumahsakit',
            name='status',
            field=models.TextField(choices=[('Buka', 'Buka'), ('Tutup', 'Tutup')], max_length=20),
        ),
        migrations.AlterField(
            model_name='rumahsakit',
            name='test_rs',
            field=models.TextField(choices=[('Iya', 'Menerima'), ('Tidak', 'Tidak Menerima')], max_length=20),
        ),
    ]
