from django.db import models
from django.core.validators import RegexValidator


# Create your models here.
class RumahSakit(models.Model):
    nama_rs = models.CharField(max_length = 100)
    link_foto = models.TextField()
    statusOpen = (
        ('Buka', 'Buka'),
        ('Tutup', 'Tutup'),
    )

    status = models.TextField(choices = statusOpen)
    link_rs = models.TextField()
    statusPasien = (
        ('Iya', 'Menerima '),
        ('Tidak', 'Tidak Menerima'),
    )   
    covid = models.TextField(choices = statusPasien)
    statusTest = (
        ('Iya', 'Menerima'),
        ('Tidak', 'Tidak Menerima'),
    )
    test_rs = models.TextField(choices = statusTest)
    alamat = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.nama_rs

class Dokter(models.Model):
    
    rs = models.ForeignKey(RumahSakit, on_delete = models.CASCADE)
    nama_dokter = models.CharField(max_length = 25)
    spesialis = models.CharField(max_length = 25)
    layanan_start = models.TimeField()
    layanan_end = models.TimeField()
    phone_number = models.IntegerField()

    def __str__(self):
        return self.nama_dokter